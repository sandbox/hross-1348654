
Synaptica for Drupal
====================

The Synaptica module provides taxonomy integration with the Synaptica taxonomy building tool (specifically the Web Service API). More information on Synaptica is available at: http://synaptica.com


INSTALLATION
------------

1. Extract the Synaptica module to your local modules directory (sites/all/modules).


CONFIGURATION
-------------
   
1. Enable the Synaptica module in:
       admin/build/modules

2. Browse to the admin page and configure your web service URL at:

3. Run the Taxonomy sync at:

